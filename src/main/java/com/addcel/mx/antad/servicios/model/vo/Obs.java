package com.addcel.mx.antad.servicios.model.vo;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("Obs")
public class Obs {

	@XStreamAsAttribute
	private String timePeriod;
	
	@XStreamAsAttribute
	private String obsValue;

	public String getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(String timePeriod) {
		this.timePeriod = timePeriod;
	}

	public String getObsValue() {
		return obsValue;
	}

	public void setObsValue(String obsValue) {
		this.obsValue = obsValue;
	}

}
