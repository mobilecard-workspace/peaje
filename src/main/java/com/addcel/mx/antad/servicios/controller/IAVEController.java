package com.addcel.mx.antad.servicios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.services.IAVEService;

@Controller
public class IAVEController {
	
	private static final String PATH_PAGO_IAVE = "{idApp}/{idioma}/iave/3dsecure/procesaPago";
	
	private static final String PATH_PROCESA_RESPUESTA_PROSA = "/iave/3dsecure/respuestaProsa";
	
	private static final String PATH_PROCESA_RESPUESTA_PROSA_DEUDOR = "/iave/3d/secure/deudor";
	
	private static final String PARAM_JSON = "json";
		
	@Autowired
	private IAVEService iaveService;
	
	@RequestMapping(value = PATH_PAGO_IAVE, method = RequestMethod.POST)
	public ModelAndView pagoIave3DS(@PathVariable int idApp, @PathVariable String idioma,
			 @RequestParam(PARAM_JSON) String json, ModelMap modelo) {
		return iaveService.pagoIave3DS(idApp, idioma, json, modelo);
	}
	
	@RequestMapping(value = PATH_PROCESA_RESPUESTA_PROSA, method = RequestMethod.POST)
	public ModelAndView respuestaProsa(@RequestParam String EM_Response,
			@RequestParam String EM_Total, @RequestParam String EM_OrderID,
			@RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum,
			@RequestParam String EM_Auth, @RequestParam String EM_Digest) {
		
		return iaveService.procesaRespuestaProsa(EM_Response,
				EM_Total, EM_OrderID, EM_Merchant, EM_Store,
				EM_Term, EM_RefNum, EM_Auth, EM_Digest);	
	}
	
	@RequestMapping(value = PATH_PROCESA_RESPUESTA_PROSA_DEUDOR, method = RequestMethod.POST)
	public ModelAndView procesaCobroDeudor(@RequestParam String EM_Response,
			@RequestParam String EM_Total, @RequestParam String EM_OrderID,
			@RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum,
			@RequestParam String EM_Auth, @RequestParam String EM_Digest) {
		return iaveService.procesaCobroDeudor(EM_Response,
				EM_Total, EM_OrderID, EM_Merchant, EM_Store,
				EM_Term, EM_RefNum, EM_Auth, EM_Digest);	
	}
		
	@RequestMapping(value = "/iave/enviaPago3DS", method = RequestMethod.POST)
	public ModelAndView enviaPago3DS(@ModelAttribute MobilePaymentRequestVO pago,  ModelMap modelo) {
		return iaveService.procesaPagoAmex(pago, modelo);
	}
	
	@RequestMapping(value = "test", method=RequestMethod.GET)
	public ModelAndView home() {	
		ModelAndView mav = new ModelAndView("home");
		return mav;	
	}
//	
//	
//	@RequestMapping(value = "/iave/3dSecure/respuestaPayworks")
//	public ModelAndView payworksRec3DRespuesta(@RequestBody String cadena, ModelMap modelo) {
//		return iaveService.procesaRespuesta3DPayworks(cadena, modelo);	
//	}
//	
//	@RequestMapping(value = "/iave/payworks/respuesta")
//	public ModelAndView payworks2RecRespuesta(
//			@RequestParam String NUMERO_CONTROL,
//			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
//			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
//			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW, @RequestParam(required = false) String BANCO_EMISOR,
//			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
//			@RequestParam String ID_AFILIACION, @RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA, 
//			ModelMap modelo,HttpServletRequest request) {
//		return iaveService.payworks2Respuesta(NUMERO_CONTROL,REFERENCIA, FECHA_RSP_CTE, 
//				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW, RESULTADO_AUT, 
//				BANCO_EMISOR, ID_AFILIACION, TIPO_TARJETA, MARCA_TARJETA, modelo);	
//	}
	
}