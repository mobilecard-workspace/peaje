package com.addcel.mx.antad.servicios.model.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.mx.antad.servicios.model.vo.AfiliacionVO;
import com.addcel.mx.antad.servicios.model.vo.AntadCredentials;
import com.addcel.mx.antad.servicios.model.vo.AntadDetalle;
import com.addcel.mx.antad.servicios.model.vo.AntadInfo;
import com.addcel.mx.antad.servicios.model.vo.BitacoraDetalleVO;
import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.Categorias;
import com.addcel.mx.antad.servicios.model.vo.DatosDevolucion;
import com.addcel.mx.antad.servicios.model.vo.PagosDirectos;
import com.addcel.mx.antad.servicios.model.vo.RecargaMontos;
import com.addcel.mx.antad.servicios.model.vo.Recargas;
import com.addcel.mx.antad.servicios.model.vo.RuleVO;
import com.addcel.mx.antad.servicios.model.vo.ServiciosAntadRequest;
import com.addcel.mx.antad.servicios.model.vo.ServiciosAntadResponse;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.mx.antad.servicios.model.vo.ValidacionVO;

public interface AntadServiciosMapper {

	public List<ServiciosAntadResponse> consultaServiciosAntad(ServiciosAntadRequest request);
	
	public List<ServiciosAntadResponse> consultaServiciosAntadIngles(ServiciosAntadRequest request);

	public int insertaBitacoraTransaccion(BitacoraVO bitacora);
	
	public int insertaBitacoraTransaccionDetalle(BitacoraDetalleVO bitacora);

	public double getComision(int idProveedor);

	public void actualizaBitacora(BitacoraVO bitacora);

	public UsuarioVO getUsuario(@Param(value = "id") long idUsuario, @Param(value="idTarjeta") int idTarjeta);
	
	public String getFechaActual();

	public void insertaAntadDetalle(AntadDetalle antadDetalle);

	public void insertaBitacoraProsa(BitacoraVO bitacora);
	
	public int addBitacoraProsa(TBitacoraProsaVO b);

	public List<Recargas> getRecargas(int idAplicacion);
	
	public List<Recargas> getRecargasEN(int idAplicacion);

	public List<Recargas> getRecargaServicios(@Param(value="id") int idRecarga);

	public List<RecargaMontos> getRecargaMontos(int idServicio);

	public List<Categorias> getServicioCategorias(int idAplicacion);
	
	public List<Categorias> getServicioCategoriasEN(int idAplicacion);

	public AfiliacionVO buscaAfiliacion(@Param(value="id") String id);
	
	public int guardaTBitacoraPIN(@Param(value="idBitacora") long idBitacora, @Param(value="pin") String pin);
	
	public String buscarTBitacoraPIN(@Param(value="idBitacora")String idBitacora);
	
	public int updateBitacora(TBitacoraVO b);
	
	public int updateBitacoraProsa(TBitacoraProsaVO b);
	
	public AntadInfo buscarDetallePago(@Param(value="idBitacora") String idBitacora);
	
	public void insertaDatosDevolucion(DatosDevolucion devolucion);
	
	public DatosDevolucion getDatosDevolucion(@Param(value="idBitacora") long idBitacora);

	public String getParametro(@Param(value = "clave") String valor);
	
	public RuleVO selectRule(@Param(value = "idUsuario") long idUsuario, @Param(value = "tarjeta") String tarjeta,
			@Param(value = "idProveedores") String idProveedores, @Param(value = "parametro") String parametro);
	
	public int getBloqueoTarjeta(@Param(value = "tarjeta") String tarjeta);
	
	public double obtenDivisa(@Param(value = "id") String string);

	public PagosDirectos getPago(@Param(value = "id")String idTransaccion);

//	public void actualizaPagoDirecto(String nUMERO_CONTROL, String rESULTADO_PAYW, String tEXTO, String cODIGO_AUT,
//			String cODIGO_PAYW, String bANCO_EMISOR);

	public void insertaPagoDirecto(PagosDirectos pago);
	
	public void actualizaPagoDirecto3D(PagosDirectos pago);
	
	public void actualizaPagoDirectoPayworks(PagosDirectos pago);
	
	public RuleVO validaIntentosFallidos(@Param(value = "id") long idUsuario, @Param(value = "idProveedores") String idProveedores,
			@Param(value = "tarjeta") String tarjeta);

	public int validaTarjetasBloqueadas(@Param(value = "id") long idUsuario);

	public void getBloqueoUsuario(@Param(value = "id") long idUsuario);

	public ArrayList<String> getTarjetasUsuario(@Param(value = "id") long idUsuario);

	public void getBloqueoIMEI(@Param(value = "imei") String imei);
	
	public void bloqueaTarjeta(@Param(value = "tarjeta") String imei);

	public String validaReglas(@Param(value = "idUsuario")long idUsuario, @Param(value = "imei")String imei, 
			@Param(value = "idTarjeta")int idTarjeta, @Param(value = "tarjeta")String tarjeta, 
			@Param(value = "operacion") String operacion);

	public AntadCredentials antadCredenciales();
	
	public UsuarioVO getDatosUsuario(@Param(value = "id") long idUsuario);

	public List<RecargaMontos> getRecargaMontosUSD(int idServicio);

}
