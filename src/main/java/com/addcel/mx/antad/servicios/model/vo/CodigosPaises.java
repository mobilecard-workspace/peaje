package com.addcel.mx.antad.servicios.model.vo;

import java.util.List;

public class CodigosPaises {

	private List<CodigoPais> paises;

	public List<CodigoPais> getPaises() {
		return paises;
	}

	public void setPaises(List<CodigoPais> paises) {
		this.paises = paises;
	}
	
}
