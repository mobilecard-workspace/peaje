package com.addcel.mx.antad.servicios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.model.vo.RespuestaAmex;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.google.gson.Gson;

@Service
public class AmexService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IAVEService.class);
	
	private static final String AMEX_WEB_URL = "http://localhost/AmexWeb/AmexAuthorization";

	private  Gson gson = new Gson();
	
	public RespuestaAmex enviaPagoAmex(MobilePaymentRequestVO pago, UsuarioVO usuarioVO) {
		RespuestaAmex respAmex = null;
		String json = null;
		try {
			json = solicitaAutorizacionAmex(pago, usuarioVO);
			respAmex = gson.fromJson(json, RespuestaAmex.class);
			LOGGER.debug("RESPUESTA AMEX - CODE: " + respAmex.getCode() + ", TRAN: " + respAmex.getTransaction() 
					+ ", DESCRIPT:  " + respAmex.getDsc()+", ERROR: "+respAmex.getError() +", ERROR DESC: "+respAmex.getErrorDsc());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return respAmex;
	}

	private String solicitaAutorizacionAmex(MobilePaymentRequestVO pago, UsuarioVO usuarioVO) {
		String json = null;
		double total = 0.0;
		try {
			total = pago.getMonto() + pago.getComision();
			LOGGER.info("SOLICITANDO AUTORIZACION AMEX - DIR: "+usuarioVO.getDom_amex());
			RestTemplate restClient = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		    map.add("tarjeta", pago.getTarjeta());
		    map.add("vigencia", pago.getVigencia().replace("/", ""));
		    map.add("producto", "iave");
		    map.add("afiliacion", "9351669578");
		    map.add("cid", pago.getCvv2());
		    map.add("monto", String.valueOf(total));
		    map.add("direccion", usuarioVO.getDireccion());
		    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		    
//			Map<String, String> params = new HashMap<String, String>();
//		    params.put("tarjeta", URLEncoder.encode(pago.getTarjeta(), "UTF-8"));
//		    params.put("vigencia", URLEncoder.encode(pago.getVigencia(), "UTF-8"));
//		    params.put("monto", URLEncoder.encode(String.valueOf(pago.getMonto()), "UTF-8"));
//		    params.put("cid", URLEncoder.encode(pago.getCvv2(), "UTF-8"));
//		    params.put("direccion", URLEncoder.encode(usuarioVO.getDireccion(), "UTF-8"));
//		    params.put("cp", URLEncoder.encode(usuarioVO.getCodigoPostal(), "UTF-8"));
		    
		    ResponseEntity<String> response  = restClient.postForEntity(AMEX_WEB_URL, request, String.class);
		    json = response.getBody();
			LOGGER.info("RESPUESTA DE AMEX: "+json);
//			json = restClient.posForEntity(AMEX_WEB_URL+"?tarjeta="+URLEncoder.encode(pago.getTarjeta(), "UTF-8")
//					+"&vigencia="+URLEncoder.encode(pago.getVigencia(), "UTF-8")
//					+"&monto="+URLEncoder.encode(String.valueOf(pago.getMonto()), "UTF-8")
//					+"&cid="+URLEncoder.encode(pago.getCvv2(), "UTF-8")
//					+"&direccion="+URLEncoder.encode("", "UTF-8")
//					+"&cp="+URLEncoder.encode("", "UTF-8"), String.class, params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	

}
