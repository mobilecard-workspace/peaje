package com.addcel.mx.antad.servicios.model.mapper;

import org.apache.ibatis.annotations.Param;

import com.addcel.mx.antad.servicios.model.vo.AfiliacionVO;
import com.addcel.mx.antad.servicios.model.vo.Deudor;
import com.addcel.mx.antad.servicios.model.vo.IaveVO;
import com.addcel.mx.antad.servicios.model.vo.Producto;
import com.addcel.mx.antad.servicios.model.vo.Proveedor;
import com.addcel.mx.antad.servicios.model.vo.ReloadResponse;
import com.addcel.mx.antad.servicios.model.vo.RuleVO;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;

public interface IaveMapper {

	public UsuarioVO validaUsuario(@Param(value = "idApp") int idApp, @Param(value = "login") String login, @Param(value="idTarjeta") int idTarjeta);
	
	public String getParametro(@Param(value = "clave") String valor);
	
	public String getComision(@Param(value = "clave") String valor);

	public RuleVO selectRule(@Param(value = "idUsuario") long idUsuario, @Param(value = "tarjeta") String tarjeta,
			@Param(value = "idProveedores") int idProveedores, @Param(value = "parametro") String parametro);

	public Producto getProductoByClaveWS(@Param(value = "producto") String producto);

	public double getComisionXProducto(@Param(value = "proveedor") int proveedor);

	public Proveedor getProveedor(@Param(value = "proveedor") int idProveedor);

	public void addBitacora(TBitacoraVO bitacora);

	public int getInfoTag(@Param(value = "tag") String tarjeta);

	public int getInfoImei(@Param(value = "imei") String imei);

	public void addBitacoraIAVE(TBitacoraVO bitacora);
	
	public String getFolioBancoIAVE();

	public void addBitacoraProsa(TBitacoraVO bitacora);
	
	public TBitacoraVO getDatosTransaccion(@Param(value = "idBitacora") long idBitacora);

	public void actualizaBitacora(TBitacoraVO bitacora);

	public void actualizaBitacoraProsa(TBitacoraVO bitacora);

	public void actualizaBitacoraIAVE(ReloadResponse response);
	
	public AfiliacionVO buscaAfiliacion(@Param(value="id") String id);
	
	public UsuarioVO getUsuario(@Param(value = "id") long idUsuario);
	
	public int updateBitacora(TBitacoraVO b);
	
	public int updateBitacoraProsa(TBitacoraProsaVO b);
	
	public int guardaTBitacoraPIN(@Param(value="idBitacora") long idBitacora, @Param(value="pin") String pin);
	
	public String buscarTBitacoraPIN(@Param(value="idBitacora")String idBitacora);
	
	public int getBloqueoTarjeta(@Param(value = "tarjeta") String tarjeta);

	public IaveVO buscarDetalleIavePago(@Param(value = "idBitacora") String string);

	public int getValidaBines(@Param(value = "bin") String tarjeta);

	public String validaIAVE(@Param(value = "idUsuario") long idUsuario, @Param(value = "proveedores") String string, 
			@Param(value = "tag") String tag, @Param(value = "imei") String imei, @Param(value = "idTarjeta") int idTarjeta, 
			@Param(value = "bin") String bin, @Param(value = "amex") int amex);
	
	public RuleVO validaCompraTag(@Param(value = "idUsuario") long idUsuario, @Param(value = "tag") String tarjeta);
	
	public Deudor validaDeudor(@Param(value = "idUsuario") long idUsuario, @Param(value = "tag") String tag, 
			@Param(value = "tarjeta") String tarjeta);
	
	public int actualizaDeudor(@Param(value = "aut") String aut, @Param(value = "idBitacora") String idBitacora);
	
	public int actualizaTransaccionDeudor(@Param(value = "id") String id, @Param(value = "idBitacora") String idBitacora,
			@Param(value = "imei") String imei);
}
