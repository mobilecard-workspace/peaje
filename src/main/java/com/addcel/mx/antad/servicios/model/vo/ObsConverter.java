package com.addcel.mx.antad.servicios.model.vo;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ObsConverter implements Converter{

	public void marshal(Object value, HierarchicalStreamWriter writer, 
            MarshallingContext context) {
		Obs obs = (Obs) value;
		writer.addAttribute("TIME_PERIOD", obs.getTimePeriod());
		writer.addAttribute("OBS_VALUE", obs.getObsValue());
		
	}
	
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		Obs obs = new Obs();
		System.out.println("OBS_VALUE: "+reader.getAttribute(1)+
				", TIME_PERIOD: "+reader.getAttribute(0));
		
        obs.setObsValue(reader.getAttribute(1));
        obs.setTimePeriod(reader.getAttribute(0));
        return obs;
    }
	
	public boolean canConvert(Class clazz) {
        return clazz.equals(Obs.class);
    }
}
