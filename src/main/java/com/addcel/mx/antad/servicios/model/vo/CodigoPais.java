package com.addcel.mx.antad.servicios.model.vo;

public class CodigoPais {

	private int id;
	
	private String codigoPais;
	
	private String nombrePais;
	
	public CodigoPais(int id, String codigoPais, String nombrePais) {
		this.id = id;
		this.codigoPais = codigoPais;
		this.nombrePais = nombrePais;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}

	public String getNombrePais() {
		return nombrePais;
	}

	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}
	
}
