package com.addcel.mx.antad.servicios.utils;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.mx.antad.servicios.client.banxico.DgieWSPortProxy;
import com.addcel.mx.antad.servicios.model.vo.CompactData;
import com.addcel.mx.antad.servicios.model.vo.DataSet;
import com.addcel.mx.antad.servicios.model.vo.Header;
import com.addcel.mx.antad.servicios.model.vo.Obs;
import com.addcel.mx.antad.servicios.model.vo.ObsConverter;
import com.addcel.mx.antad.servicios.model.vo.Sender;
import com.addcel.mx.antad.servicios.model.vo.Series;
import com.thoughtworks.xstream.XStream;

public class DivisaUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(DivisaUtils.class);
	
	private static DgieWSPortProxy proxy = null;
	
	private static XStream xStream;
	
	static {
		proxy = new DgieWSPortProxy();
	}
	
	private static XStream get() {
        if (xStream == null) {
            xStream = new XStream();
            xStream.alias("CompactData", CompactData.class);
        }
        return xStream;
    }
	
	public double consultaDivisa(){
		double dolar = 0;
		String result = null;
		CompactData data = null;
		try {
			result = proxy.tiposDeCambioBanxico();
			LOGGER.info("Resultado de la consulta: "+result);
			result = result.replace("bm:DataSet", "DataSet");
			result = result.replace("bm:SiblingGroup", "SiblingGroup");
			result = result.replace("bm:Series", "Series");
			result = result.replace("bm:Obs", "Obs");
			xStream = new XStream();
			
			xStream.alias("CompactData", CompactData.class);
			xStream.alias("Header", Header.class); 
			xStream.alias("DataSet", DataSet.class); 
			xStream.alias("Sender", Sender.class); 
			xStream.alias("Series", Series.class);  
			xStream.addImplicitCollection(DataSet.class, "Series", Series.class);
			xStream.alias("Obs", Obs.class); 

			xStream.useAttributeFor(Obs.class, "timePeriod");
			xStream.useAttributeFor(Obs.class, "obsValue");
			xStream.registerConverter(new ObsConverter());
						
			data = (CompactData) get().fromXML(result);
			
			ArrayList<Series> series = data.getDataSet().getSeries();
			Series serie = series.get(1);
			dolar = Double.valueOf(serie.getObs().getObsValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dolar;
	}
	
}
