package com.addcel.mx.antad.servicios.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class RecargasResponse extends AbstractVO {

	private List<Recargas> recargas;
	
	private List<Recargas> servicios;

	private List<RecargaMontos> montos;
	
	private List<Categorias> categorias;
	
	public List<Recargas> getRecargas() {
		return recargas;
	}

	public void setRecargas(List<Recargas> recargas) {
		this.recargas = recargas;
	}

	public List<Recargas> getServicios() {
		return servicios;
	}

	public void setServicios(List<Recargas> servicios) {
		this.servicios = servicios;
	}

	public List<RecargaMontos> getMontos() {
		return montos;
	}

	public void setMontos(List<RecargaMontos> montos) {
		this.montos = montos;
	}

	public List<Categorias> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categorias> categorias) {
		this.categorias = categorias;
	}
	
}
