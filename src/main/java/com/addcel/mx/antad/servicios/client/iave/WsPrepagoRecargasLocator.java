/**
 * WsPrepagoRecargasLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.mx.antad.servicios.client.iave;

public class WsPrepagoRecargasLocator extends org.apache.axis.client.Service implements com.addcel.mx.antad.servicios.client.iave.WsPrepagoRecargas {

    public WsPrepagoRecargasLocator() {
    }


    public WsPrepagoRecargasLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WsPrepagoRecargasLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for wsPrepagoRecargasSoap
//    private java.lang.String wsPrepagoRecargasSoap_address = "http://200.57.40.42/wsPrepagoRecargas_prueba/Service.asmx";
    private java.lang.String wsPrepagoRecargasSoap_address = "http://10.0.0.42/wsPrepagoRecargas/Service.asmx";

    public java.lang.String getwsPrepagoRecargasSoapAddress() {
        return wsPrepagoRecargasSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String wsPrepagoRecargasSoapWSDDServiceName = "wsPrepagoRecargasSoap";

    public java.lang.String getwsPrepagoRecargasSoapWSDDServiceName() {
        return wsPrepagoRecargasSoapWSDDServiceName;
    }

    public void setwsPrepagoRecargasSoapWSDDServiceName(java.lang.String name) {
        wsPrepagoRecargasSoapWSDDServiceName = name;
    }

    public com.addcel.mx.antad.servicios.client.iave.WsPrepagoRecargasSoap getwsPrepagoRecargasSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(wsPrepagoRecargasSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getwsPrepagoRecargasSoap(endpoint);
    }

    public com.addcel.mx.antad.servicios.client.iave.WsPrepagoRecargasSoap getwsPrepagoRecargasSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.mx.antad.servicios.client.iave.WsPrepagoRecargasSoapStub _stub = new com.addcel.mx.antad.servicios.client.iave.WsPrepagoRecargasSoapStub(portAddress, this);
            _stub.setPortName(getwsPrepagoRecargasSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setwsPrepagoRecargasSoapEndpointAddress(java.lang.String address) {
        wsPrepagoRecargasSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.mx.antad.servicios.client.iave.WsPrepagoRecargasSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.mx.antad.servicios.client.iave.WsPrepagoRecargasSoapStub _stub = new com.addcel.mx.antad.servicios.client.iave.WsPrepagoRecargasSoapStub(new java.net.URL(wsPrepagoRecargasSoap_address), this);
                _stub.setPortName(getwsPrepagoRecargasSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("wsPrepagoRecargasSoap".equals(inputPortName)) {
            return getwsPrepagoRecargasSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.idmexico.com.mx/wsPrepagoRecargas", "wsPrepagoRecargas");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.idmexico.com.mx/wsPrepagoRecargas", "wsPrepagoRecargasSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("wsPrepagoRecargasSoap".equals(portName)) {
            setwsPrepagoRecargasSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
