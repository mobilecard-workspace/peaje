package com.addcel.mx.antad.servicios.services;

import static com.addcel.mx.antad.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.antad.servicios.client.iave.ObjectFactory;
import com.addcel.mx.antad.servicios.client.iave.Reload;
import com.addcel.mx.antad.servicios.client.iave.WsPrepagoRecargasSoapProxy;
import com.addcel.mx.antad.servicios.model.mapper.IaveMapper;
import com.addcel.mx.antad.servicios.model.vo.AbstractVO;
import com.addcel.mx.antad.servicios.model.vo.AfiliacionVO;
import com.addcel.mx.antad.servicios.model.vo.BalanceResponse;
import com.addcel.mx.antad.servicios.model.vo.DatosCorreoVO;
import com.addcel.mx.antad.servicios.model.vo.DatosDevolucion;
import com.addcel.mx.antad.servicios.model.vo.Deudor;
import com.addcel.mx.antad.servicios.model.vo.IaveVO;
import com.addcel.mx.antad.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.antad.servicios.model.vo.ProcomVO;
import com.addcel.mx.antad.servicios.model.vo.Producto;
import com.addcel.mx.antad.servicios.model.vo.ReglasResponse;
import com.addcel.mx.antad.servicios.model.vo.ReloadResponse;
import com.addcel.mx.antad.servicios.model.vo.ResponsePagoMovilesVO;
import com.addcel.mx.antad.servicios.model.vo.RespuestaAmex;
import com.addcel.mx.antad.servicios.model.vo.RespuestaServicioVO;
import com.addcel.mx.antad.servicios.model.vo.TBitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.mx.antad.servicios.model.vo.ValidacionVO;
import com.addcel.mx.antad.servicios.utils.AddCelGenericMail;
import com.addcel.mx.antad.servicios.utils.Constantes;
import com.addcel.mx.antad.servicios.utils.ParseIaveResponse;
import com.addcel.mx.antad.servicios.utils.Utils;
import com.addcel.mx.antad.servicios.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;

@Service
public class IAVEService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IAVEService.class);
	
	private static final String URL_CHECK_BALANCE = "http://localhost/GestionUsuarios/es/{idUsuario}/tags/checkBalance";
	
	private static final String TELEFONO_SERVICIO = "5525963513";
	
	private static final String AMEX_AUTHORIZATION_CODE = "000";
	
	private static DecimalFormat DF;
	
	@Autowired
	private UtilsService jsonUtils;
	
	@Autowired
	private IaveMapper mapper;

	/**
	 * METODO PARA REALIZAR PAGOS CON PROSA HSBC
	 * 
	 * @param json
	 * @param modelo
	 * @return
	 */
	public ModelAndView pagoIave3DS(int idApp, String idioma, String json, ModelMap modelo) {
		ModelAndView mav = null;
		UsuarioVO usuario = null;
		ProcomVO procom = null;
		RespuestaServicioVO respuesta = null;
		IaveVO compra = null;
		ReglasResponse response = null;
		Producto producto = null;
		AfiliacionVO afiliacion = null;
		MobilePaymentRequestVO pago = new MobilePaymentRequestVO();
		Deudor deudor = null;
		double comision = 0;
		double total = 0;
		try {
			LOGGER.info("JSON PAGO IAVE - "+ json);
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info("JSON PAGO IAVE - "+ json);
			compra = (IaveVO) jsonUtils.jsonToObject(json, IaveVO.class);
			LOGGER.info("INICIANDO PAGO IAVE 3D SECURE: ID USUARIO - {}", compra.getLogin());
			respuesta = new RespuestaServicioVO();
			LOGGER.info("VALIDANDO ID TARJETA: "+compra.getIdTarjeta());
    		if(compra.getIdTarjeta() == 0){
    			LOGGER.info("VERSION DEL APP DESACTUALIZADA, DEBE ACTUALIZAR LA VERSION.");
    			respuesta.setIdError(-1);
				respuesta.setMsgError("Estimado usuario, hemos detectado que tiene una version antigua de Mobilecard, "
    					+ "por favor, actualicela para poder hacer uso de los servicios que le ofrece Mobilecard. Por su atencion, gracias. ");
    		} else {
	    		mav = new ModelAndView("payworks/pagina-3DS");
	    		LOGGER.info("INICIANDO PAGO IAVE 3D SECURE: ID USUARIO - {}", compra.getLogin());
	    		usuario = mapper.validaUsuario(idApp, compra.getLogin(), compra.getIdTarjeta());
				LOGGER.info("DATOS  DE USUARIO: "+usuario.getNombre()+",  ID TARJETA: "+pago.getIdTarjeta());
				if(usuario != null){
					deudor = mapper.validaDeudor(compra.getIdUsuario(), compra.getTarjeta(), AddcelCrypto.decryptTarjeta(usuario.getNumTarjeta()));
					if ("1".equals(usuario.getTipoTarjeta()) || "2".equals(usuario.getTipoTarjeta())
							|| "11".equals(usuario.getTipoTarjeta())){
						LOGGER.debug("IAVE validacion Reglas negocio, idUsuario: " + usuario.getIdUsuario()+" "+ setSMS(compra.getTarjeta()));
						if(!compra.isDebug()) {
							response = validaReglas(usuario.getIdUsuario(), setSMS(compra.getTarjeta()), 0, usuario.getNumTarjeta(), 
									compra.getImei(), compra.getIdTarjeta(), 0);
						}
						
						if(response == null){
							producto = mapper.getProductoByClaveWS(compra.getProducto());
							comision = mapper.getComisionXProducto(Integer.parseInt(producto.getProveedor()));
							compra.setComision(Double.valueOf(comision));
							if(compra.isDebug()) {
								total = 1;
								producto.setMonto("1");
								compra.setComision(0);
							} else if(deudor != null) {
								total = deudor.getMonto() +7;
								producto.setMonto(String.valueOf(deudor.getMonto()));
								compra.setComision(7);
							}else {
								total = Double.valueOf(producto.getMonto())+comision;
							}
							insertaBitacoras(idApp, compra, usuario, producto);
							afiliacion = mapper.buscaAfiliacion("2");
							procom = new ProcomService().comercioFin(compra.getIdBitacora(), 
									total, afiliacion);
							if(deudor != null) {
								LOGGER.info("ES DEUDOR: ID: "+deudor.getId()+", ID_USUARIO: "+deudor.getIdUsuario()
								+", TAG: "+deudor.getTag()+", TARJETA: "+deudor.getTarjeta()+", MONTO: "+deudor.getMonto());
								total = deudor.getMonto()+7;
								LOGGER.info("TOTAL DEUDOR: "+procom.getTotal());
								procom.setUrlBack("https://www.mobilecard.mx/Peaje/iave/3d/secure/deudor");
								mapper.actualizaTransaccionDeudor(String.valueOf(deudor.getId()), 
										String.valueOf(compra.getIdBitacora()), compra.getImei());
							}
						} else {
							respuesta.setIdError(-1);
							respuesta.setMsgError("Disculpe las molestias, "+response.getMensaje());
							LOGGER.info("TRANSACCION RECHAZADA - REGLAS DE NEGOCIO - "+response.getMensaje());
							json =  jsonUtils.objectToJson(response);
						}
					} else if ("3".equals(usuario.getTipoTarjeta())) {
						response = validaReglas(usuario.getIdUsuario(), setSMS(compra.getTarjeta()), 0, usuario.getNumTarjeta(), 
								compra.getImei(), compra.getIdTarjeta(), 1);
						if(response == null){
							producto = mapper.getProductoByClaveWS(compra.getProducto());
							comision = mapper.getComisionXProducto(Integer.parseInt(producto.getProveedor()));
							compra.setComision(Double.valueOf(comision));
							insertaBitacoras(idApp, compra, usuario, producto);
							total = Double.valueOf(producto.getMonto())+comision;
							pago.setNombre(usuario.getNombre() );
				    		pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuario.getNumTarjeta()));
				    		pago.setTarjeta("XXXX XXXX XXXX " + 
	                				pago.getTarjeta().substring(pago.getTarjeta().length() - 4 ,pago.getTarjeta().length()));
				    		pago.setTarjetaT("AMERICAN EXPRESS");
				    		pago.setVigencia(AddcelCrypto.decryptTarjeta(usuario.getVigenciaTarjeta()));
				    		if(usuario.getCvv2() != null){
				    			pago.setCvv2(AddcelCrypto.decryptTarjeta(usuario.getCvv2()));
				    		}
				    		pago.setTipoTarjeta(Integer.valueOf(usuario.getTipoTarjeta()));
				    		pago.setMonto(Double.valueOf(producto.getMonto()));
	                		pago.setComision(comision);
	                		pago.setLogin(compra.getLogin());
	                		pago.setIdTransaccion(compra.getIdBitacora());
	                		pago.setTipoPago("AMEX");
	                		pago.setIdTarjeta(compra.getIdTarjeta());
	                		modelo.put("pagoForm", pago);
						} else {
							respuesta.setIdError(-1);
							respuesta.setMsgError("Disculpe las molestias, "+response.getDescripcionRechazo());
							LOGGER.error("Error Intento de pago AMEX.");
						}
					} else {
						respuesta.setIdError(-1);
						respuesta.setMsgError("Tipo de Tarjeta no valida.");
						LOGGER.error("Error tipo de tarjeta registrada: {}" + usuario.getTipoTarjeta());
					}
				} else {
					respuesta.setIdError(-100);
					respuesta.setMsgError("Usuario no encontrado");
				}
    		}
		} catch (Exception e) {
			respuesta.setIdError(-100);
			respuesta.setMsgError("Ocurrio un error al realizar el pago del servicio, problemas internos -3");
			LOGGER.error("Error al realizar la recarga", e);
		} finally {
			if (respuesta.getIdError() == 0) {
				LOGGER.info("ENVIADO INFORMACION A 3DSECURE - ID TRANSACCION: "+compra.getIdBitacora());
				if("3".equals(usuario.getTipoTarjeta())){
					mav = new ModelAndView("payworks/iave3ds");
        			mav.addObject("pagoForm", pago);
				} else {
					if(usuario.getCvv2() != null && StringUtils.isNotEmpty(usuario.getCvv2())){
                		pago.setCvv2(AddcelCrypto.decryptTarjeta(usuario.getCvv2()));
					}
					mav = new ModelAndView("3ds/comerciofin");
					mav.addObject("prosa", procom);
				}
			} else {
				mav = new ModelAndView("3ds/error");
				mav.addObject("descError", respuesta.getMsgError());
			}
		}
		return mav;
	}
	
	public ModelAndView procesaPagoAmex(MobilePaymentRequestVO pago, ModelMap modelo){
		ModelAndView mav = null;
		AfiliacionVO afiliacion = null;
		UsuarioVO usuarioVO = null;
		try{
			usuarioVO = mapper.validaUsuario(1, pago.getLogin(), pago.getIdTarjeta());
			LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+",  ID TARJETA: "+pago.getIdTarjeta());
			pago.setNombre(usuarioVO.getNombre());
    		pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
    		pago.setVigencia(AddcelCrypto.decryptTarjeta(usuarioVO.getVigenciaTarjeta()));
    		pago.setTipoTarjeta(Integer.valueOf(usuarioVO.getTipoTarjeta()));
			if("AMEX".equals(pago.getTipoPago())){
				mav = compraIAVExAmex(pago, usuarioVO);
			} else {
				mav = new ModelAndView("exito");
				afiliacion = mapper.buscaAfiliacion("3");
				pago.setAfiliacion(afiliacion);
				if(afiliacion == null){
					LOGGER.debug("NO hay afiliacion configurada" );
					mav = new ModelAndView("payworks/iaveError");
					mav.addObject("descError", "No existe una afiliacion configurada.");
				}else{
					mav = procesaPago3DSecurePayWorks(pago, modelo);
				}
			}
		}catch(Exception e){
			LOGGER.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			e.printStackTrace();
			mav = new ModelAndView("payworks/iaveError");
			mav.addObject("descError", "Error General en el proceso de pago: " + e.getMessage());
		}
		return mav;
	}
	
	public ModelAndView procesaPago(Integer idApp, String idioma, String json, ModelMap modelo) {
		MobilePaymentRequestVO pago = null;
		ModelAndView mav = null;
		IaveVO compra = null;
		UsuarioVO usuarioVO = null;
		ReglasResponse response = null;
		AbstractVO resValidacion = null;
		Producto producto = null;
		double comision = 0;
		try {
			LOGGER.info("JSON PAGO IAVE - "+ json);
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info("JSON PAGO IAVE - "+ json);
			compra = (IaveVO) jsonUtils.jsonToObject(json, IaveVO.class);
			LOGGER.info("INICIANDO PAGO IAVE 3D SECURE: ID USUARIO - {}", compra.getLogin());
    		mav = new ModelAndView("payworks/iave3ds");
    		if(compra.getIdTarjeta() == 0){
    			mav = new ModelAndView("payworks/iaveError");
    			mav.addObject("descError", "Estimado usuario, hemos detectado que tiene una version antigua de Mobilecard, "
    					+ "por favor, actualicela para poder hacer uso de los servicios que le ofrece Mobilecard. Por su atencion, gracias. ");
    			return mav;
    		}
    		usuarioVO = mapper.validaUsuario(idApp, compra.getLogin(), compra.getIdTarjeta());
    		LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+", TARJETA: "+usuarioVO.getNumTarjeta()
    				+", VIGENCIA: "+usuarioVO.getVigencia()+", CVV: "+usuarioVO.getCvv2()+",  ID TARJETA: "+compra.getIdTarjeta());
    		if ("1".equals(usuarioVO.getTipoTarjeta()) || "2".equals(usuarioVO.getTipoTarjeta()) 
    				|| "3".equals(usuarioVO.getTipoTarjeta())){
    			response = validaReglas(usuarioVO.getIdUsuario(), usuarioVO.getNumTarjeta(), 0, usuarioVO.getNumTarjeta(), compra.getImei(), compra.getIdTarjeta(), 0);
    			if(response == null){
    				resValidacion = validaBloqueos(usuarioVO.getNumTarjeta());
    				if(resValidacion.getIdError() == 0){
    					producto = mapper.getProductoByClaveWS(compra.getProducto());
						comision = mapper.getComisionXProducto(Integer.parseInt(producto.getProveedor()));
						compra.setComision(Double.valueOf(comision));
						insertaBitacoras(idApp, compra, usuarioVO, producto);
						pago = new MobilePaymentRequestVO();
						if("3".equals(usuarioVO.getTipoTarjeta())){
    						pago.setTipoPago("AMEX");
    					}
    					pago.setNombre(usuarioVO.getNombre());
                		pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
                		pago.setTarjeta("XXXX XXXX XXXX " + 
                				pago.getTarjeta().substring(pago.getTarjeta().length() - 4 ,pago.getTarjeta().length()));
                		if("1".equals(usuarioVO.getTipoTarjeta())){
                			pago.setTarjetaT("VISA");
                		} else if("2".equals(usuarioVO.getTipoTarjeta())){
                			pago.setTarjetaT("MASTERCARD");
                		} else if("3".equals(usuarioVO.getTipoTarjeta())){
                			pago.setTarjetaT("AMERICAN EXPRESS");
                		}
                		pago.setMonto(Double.valueOf(producto.getMonto()));
                		pago.setComision(comision);
                		pago.setLogin(compra.getLogin());
                		pago.setVigencia(AddcelCrypto.decryptTarjeta(usuarioVO.getVigenciaTarjeta()));
                		if(usuarioVO.getCvv2() != null && StringUtils.isNotEmpty(usuarioVO.getCvv2())){
                			pago.setCvv2(AddcelCrypto.decryptTarjeta(usuarioVO.getCvv2()));
                		}
                		pago.setIdTransaccion(compra.getIdBitacora());
                		pago.setIdTarjeta(compra.getIdTarjeta());
                		LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+",  ID TARJETA: "+compra.getIdTarjeta());
                		modelo.put("pagoForm", pago);
            			mav.addObject("pagoForm", pago);
    				} else {
    					mav = new ModelAndView("payworks/iaveError");
            			mav.addObject("descError", resValidacion.getMensajeError());
    				}
    			} else {
    				mav = new ModelAndView("payworks/iaveError");
        			mav.addObject("descError", response.getMensaje());
    			}
    		} else {
    			mav = new ModelAndView("payworks/iaveError");
    			mav.addObject("descError", "Tarjeta no valida");
    		}
		}catch (Exception pe) {
			LOGGER.error("Error General en el proceso de pago.: {}", pe.getMessage());
			pe.printStackTrace();
			mav = new ModelAndView("payworks/iaveError");
			mav.addObject("descError", "Error General en el proceso de pago: " + pe.getMessage());
		}
		return mav;
	}
	
	public ModelAndView procesaPago(Integer idApp, String idioma, MobilePaymentRequestVO pago, ModelMap modelo){
		ModelAndView mav = null;
		AfiliacionVO afiliacion = null;
		UsuarioVO usuarioVO = null;
		try{
			usuarioVO = mapper.validaUsuario(idApp, pago.getLogin(), pago.getIdTarjeta());
			LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+",  ID TARJETA: "+pago.getIdTarjeta());
			pago.setNombre(usuarioVO.getNombre());
    		pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
    		pago.setVigencia(AddcelCrypto.decryptTarjeta(usuarioVO.getVigenciaTarjeta()));
    		if(usuarioVO.getCvv2() != null && StringUtils.isNotEmpty(usuarioVO.getCvv2())){
    			pago.setCvv2(AddcelCrypto.decryptTarjeta(usuarioVO.getCvv2()));
    		}
    		pago.setTipoTarjeta(Integer.valueOf(usuarioVO.getTipoTarjeta()));
			if("AMEX".equals(pago.getTipoPago())){
				mav = compraIAVExAmex(pago, usuarioVO);
			} else {
				mav = new ModelAndView("exito");
				afiliacion = mapper.buscaAfiliacion("6");
				pago.setAfiliacion(afiliacion);
				if(afiliacion == null){
					LOGGER.debug("NO hay afiliacion configurada" );
					mav = new ModelAndView("payworks/iaveError");
					mav.addObject("descError", "No existe una afiliacion configurada.");
				}else{
					mav = procesaPago3DSecurePayWorks(pago, modelo);
				}
			}
		}catch(Exception e){
			LOGGER.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			e.printStackTrace();
			mav = new ModelAndView("payworks/iaveError");
			mav.addObject("descError", "Error General en el proceso de pago: " + e.getMessage());
		}
		return mav;
	}
	
	private ModelAndView compraIAVExAmex(MobilePaymentRequestVO pago, UsuarioVO usuarioVO) {
		AmexService amexService = new AmexService();
		RespuestaAmex respAmex = null;
		ModelAndView mav = null;
		ReloadResponse response = null;
		TBitacoraVO transaccion = null;
		double total = 0;
		try {
			respAmex =  amexService.enviaPagoAmex(pago, usuarioVO);
			transaccion = mapper.getDatosTransaccion(pago.getIdTransaccion());
			LOGGER.info("Consulta detalle pago: "+transaccion);	
			if(AMEX_AUTHORIZATION_CODE.equals(respAmex.getCode())){
				response = aprovisionaIAve(transaccion);
				if("00".equals(response.getRESPONSECODE())){
					LOGGER.info("RECARGA I+D AMEX EXITOSA... SE PROCEDE A ACTUALIZAR BITACORAS..");
					total = Double.valueOf(transaccion.getCargo()) + transaccion.getComision();
					LOGGER.info("transaccion.getBitCargo(): "+transaccion.getCargo());
					LOGGER.info("transaccion.getComision(): "+transaccion.getComision());
					mav = new ModelAndView("payworks/iavePagoExito");
					mav.addObject("respuesta", "Aprobado");
					mav.addObject("idTransaccion", pago.getIdTransaccion());	
					mav.addObject("autorizacion", respAmex.getTransaction());
					mav.addObject("importe", transaccion.getCargo());
					mav.addObject("comision", transaccion.getComision());
					mav.addObject("total", total);
					mav.addObject("descripcionCode", response.getDESCRIPTIONCODE());
					mav.addObject("tag", response.getCARD_NUMBER());
					mav.addObject("fecha", UtilsService.getFechaActual());
					String ticket = "COMPRA TAG I+D AUTORIZACION: " + response.getAUTONO() + " TAG: " + transaccion.getDestino()
							+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
					transaccion.setBitTicket(ticket);
					transaccion.setBitStatus(1);
					transaccion.setBitNoAutorizacion(respAmex.getTransaction());
					transaccion.setBitStatus(1);
					actualizaBitacora(transaccion, respAmex.getTransaction(), "1", 1, response);
					LOGGER.info("RECARGA I+D AMEX EXITOSA... SE HAN ACTUALIZADO LAS BITACORAS..");
					enviaCorreo(transaccion, response, total, String.valueOf(pago.getIdTransaccion()), respAmex.getTransaction(), null);
				} else {
					mav = new ModelAndView("payworks/iaveError");
					mav.addObject("descError", "Error IAVE: "+response.getRESPONSECODE()+" - Descripcion: " + response.getDESCRIPTIONCODE());
					String ticket = "COMPRA TAG I+D ERROR: " + response.getRESPONSECODE() + " TAG: " + transaccion.getDestino()
							+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
					transaccion.setBitTicket(ticket);
					transaccion.setBitStatus(3);
					transaccion.setBitNoAutorizacion(response.getRESPONSECODE());
					transaccion.setBitCodigoError(-1);
					actualizaBitacora(transaccion, respAmex.getTransaction(), "-1", 0, response);
				}
			} else {
				mav = new ModelAndView("payworks/iaveError");
				mav.addObject("descError", "Pago Amex: " + respAmex.getCode() + " - Descripcion: " + respAmex.getDsc());
				LOGGER.error("Error Pago 3D Secure: " + respAmex.getError() + " - " + pago.getIdTransaccion());
				String ticket = "Tarjeta rechazada  TAG: " + pago.getTarjetaT()
						+ " RECARGA DE: " + pago.getMonto() + " COMISION: " + pago.getComision();
				transaccion.setBitTicket(ticket);
				transaccion.setBitStatus(2);
				transaccion.setBitNoAutorizacion(null);
				transaccion.setBitCodigoError(-1);
				actualizaBitacora(transaccion, String.valueOf(pago.getIdTransaccion()), "-1", 0, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL APROVISIONAR O ACTUALIZAR BITACORAS: "+e.getMessage());
		}
		return mav;
	}

	public ModelAndView procesaPago3DSecurePayWorks(MobilePaymentRequestVO pago, ModelMap modelo) {
		ModelAndView mav = null;
		HashMap<String, String> resp = new HashMap<String, String>();
		int respBD = 0;
		double total = 0;
		try{
			LOGGER.info("DATOS ENVIADOS A https://eps.banorte.com/secure3d/Solucion3DSecure.htm : "+pago.toTrace());
			resp.put("ID_AFILIACION", pago.getAfiliacion().getIdAfiliacion());
			resp.put("USUARIO", pago.getAfiliacion().getUsuario());
			resp.put("CLAVE_USR", pago.getAfiliacion().getClaveUsuario());
			resp.put("CMD_TRANS", "VENTA");
			resp.put("ID_TERMINAL", pago.getAfiliacion().getIdTerminal());
			resp.put("Reference3D", pago.getIdTransaccion() + "");
			resp.put("MerchantId", pago.getAfiliacion().getMerchantId());
			resp.put("ForwardPath", pago.getAfiliacion().getForwardPath());
//			resp.put("Expires", pago.getMes() + "/" + pago.getAnio().substring(2, 4));
			resp.put("Expires", pago.getVigencia());
			total = pago.getMonto() + pago.getComision();
			resp.put("Total", Utils.formatoMontoPayworks(total + ""));
			resp.put("Total", "1");
			resp.put("Card", pago.getTarjeta());
			resp.put("CardType", pago.getTipoTarjeta() == 1? "VISA": "MC");
			respBD = mapper.guardaTBitacoraPIN(pago.getIdTransaccion(), Utils.encryptHard(pago.getCvv2()));
			if(respBD == 1){
				mav = new ModelAndView("payworks/iaveComercioSend");
				mav.addObject("prosa", resp);
				mav.addObject("pagoRequest", pago);
			}
		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav = new ModelAndView("payworks/iaveError");
			mav.addObject("descError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	public ModelAndView procesaRespuesta3DPayworks(String cadena, ModelMap modelo) {
		ResponsePagoMovilesVO datosResp = null;
		ModelAndView mav = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		String pin = null;
		String msg = null;
		AfiliacionVO afiliacion = null;
		DatosDevolucion devolucion = null;
		TBitacoraVO transaccion = null;
		try{
			cadena = cadena.replace(" ", "+");
			LOGGER.debug("Cadena procesaRespuesta3DPayworks: " + cadena);
			String[] spl = cadena.split("&");
			String[] data = null;
			for(String cad: spl){
				LOGGER.debug("data:  " + cad);
				data = cad.split("=");
				resp.put(data[0], data.length >= 2? data[1]: "");
			}
			transaccion = mapper.getDatosTransaccion(Long.valueOf((String) resp.get("Reference3D")));
			if(resp.containsKey("Status") && "200".equals(resp.get("Status"))){ 
				afiliacion = mapper.buscaAfiliacion("6");
				pin = mapper.buscarTBitacoraPIN((String) resp.get("Reference3D"));
				pin = AddcelCrypto.decryptHard(pin);
				mav=new ModelAndView("payworks/iaveComercioPAYW2");
				mav.addObject("prosa", resp);
				resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
				resp.put("USUARIO", afiliacion.getUsuario());
				resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
				resp.put("CMD_TRANS", "VENTA");
				resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
				resp.put("Total", Utils.formatoMontoPayworks((String)resp.get("Total")));
				resp.put("MODO", "PRD");
				resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
				resp.put("NUMERO_TARJETA", resp.get("Number"));
				resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
				resp.put("CODIGO_SEGURIDAD", pin);
				resp.put("MODO_ENTRADA", "MANUAL");
				resp.put("URL_RESPUESTA", Constantes.VAR_IAVE_PAYW_URL_BACK_W2);
				resp.put("IDIOMA_RESPUESTA", "ES");
				devolucion = new DatosDevolucion();
				if(resp.get("XID") != null){
					resp.put("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
					resp.put("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
					devolucion.setXid((String)resp.get("XID"));
					devolucion.setCavv((String)resp.get("CAVV"));
				}
				resp.put("CODIGO_SEGURIDAD", pin);
				devolucion = new DatosDevolucion();
				devolucion.setIdTransaccion(transaccion.getIdBitacora());
				devolucion.setXid("");
				devolucion.setCavv("");
				devolucion.setEci((String)resp.get("ECI"));
//				mapper.insertaDatosDevolucion(devolucion);
				LOGGER.info("DATOS ENVIADO A PAYWORK2: "+resp.toString());
			} else {
				mav=new ModelAndView("payworks/iaveError");
				datosResp = new ResponsePagoMovilesVO();
				msg = "TRANSACCION RECHAZADA: " + resp.get("Status") ;
				datosResp.setIdError(Integer.parseInt((String)resp.get("Status")));
				datosResp.setMensajeError("El pago fue rechazado. " + 
						(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + Constantes.errorPayW.get(resp.get("Status")) );
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setIdTransaccion(transaccion.getIdBitacora());
				transaccion.setBitTicket("Transaccion Rechazada 3DSecure");
				transaccion.setBitStatus(2);
				transaccion.setBitNoAutorizacion("");
				transaccion.setBitCodigoError(-1);
				actualizaBitacora(transaccion, (String) resp.get("Status"), "-1", 0, null);
				mav.addObject("descError", datosResp.getMensajeError());
			}
		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav=new ModelAndView("payworks/iaveError");
			mav.addObject("descError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	public ModelAndView payworks2Respuesta(String NUMERO_CONTROL,
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String CODIGO_PAYW, String RESULTADO_AUT, String BANCO_EMISOR,
			String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA,
			ModelMap modelo) {
		ModelAndView mav = null;
		ResponsePagoMovilesVO datosResp = null;
		TBitacoraVO transaccion = null;
		ReloadResponse response = null;
		try{
			LOGGER.info("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+" REFERENCIA: " + REFERENCIA
					+" FECHA_RSP_CTE: " + FECHA_RSP_CTE+" TEXTO: " + TEXTO+" RESULTADO_AUT: " + RESULTADO_AUT
					+" RESULTADO_PAYW: " + RESULTADO_PAYW+" FECHA_REQ_CTE: " + FECHA_REQ_CTE+" CODIGO_PAYW: " + CODIGO_PAYW
					+" CODIGO_AUT: " + CODIGO_AUT+" BANCO_EMISOR: " + BANCO_EMISOR+" TIPO_TARJETA: " + TIPO_TARJETA
					+" MARCA_TARJETA: " + MARCA_TARJETA);			
			if(MARCA_TARJETA != null){
				MARCA_TARJETA = MARCA_TARJETA.replaceAll("-", "");
				TIPO_TARJETA = MARCA_TARJETA + "/" + TIPO_TARJETA;
			}
			//Comentar datos cuando en PROD, cuando paywork regrese informacion
			if (StringUtils.equals(Constantes.MODO, Constantes.AUT)) {
				BANCO_EMISOR = "BANORTE";
				TIPO_TARJETA = "VISA/CREDITO";
			}
			TEXTO = Utils.cambioAcento(TEXTO);
			transaccion = mapper.getDatosTransaccion(Long.valueOf(NUMERO_CONTROL));
			LOGGER.info("Consulta detalle pago: "+transaccion);			
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				LOGGER.debug("TRANSACCION - " + NUMERO_CONTROL+" Aprobada - AUTORIZACION - "+NUMERO_CONTROL);
				double total = 0;
				total = Double.valueOf(transaccion.getCargo());
				total = transaccion.getCargo() + transaccion.getComision();
				response = aprovisionaIAve(transaccion);
				if("00".equals(response.getRESPONSECODE())){
					mav = new ModelAndView("payworks/iavePagoExito");
					mav.addObject("respuesta", "Aprobado");
					mav.addObject("idTransaccion", NUMERO_CONTROL);	
					mav.addObject("autorizacion", REFERENCIA);		
					mav.addObject("importe", transaccion.getCargo());
					mav.addObject("comision", transaccion.getComision());
					mav.addObject("total", total);
					mav.addObject("descripcionCode", response.getDESCRIPTIONCODE());
					mav.addObject("tag", response.getCARD_NUMBER());
					mav.addObject("fecha", UtilsService.getFechaActual());
					String ticket = "COMPRA TAG I+D AUTORIZACION: " + response.getAUTONO() + " TAG: " + transaccion.getDestino()
							+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
					transaccion.setBitTicket(ticket);
					transaccion.setBitStatus(1);
					transaccion.setBitNoAutorizacion(REFERENCIA);
					
					actualizaBitacora(transaccion, REFERENCIA, "0", 1, response);
					enviaCorreo(transaccion, response, total, REFERENCIA, NUMERO_CONTROL, null);
				} else {
					mav = new ModelAndView("payworks/iaveError");
					mav.addObject("descError", "Error IAVE: "+response.getRESPONSECODE()+" - Descripcion: " + response.getDESCRIPTIONCODE());
					String ticket = "COMPRA TAG I+D ERROR: " + response.getRESPONSECODE() + " TAG: " + transaccion.getDestino()
							+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
					transaccion.setBitTicket(ticket);
					transaccion.setBitStatus(3);
					transaccion.setBitNoAutorizacion(response.getRESPONSECODE());
					transaccion.setBitCodigoError(-1);
					actualizaBitacora(transaccion, REFERENCIA, "-1", 0, response);
				}
			} else {
				mav = new ModelAndView("payworks/iaveError");
				mav.addObject("descError", "Error Pago 3D Secure: " + RESULTADO_PAYW + " - Descripcion: " + TEXTO);
				LOGGER.error("Error Pago 3D Secure: " + RESULTADO_PAYW + " - " + REFERENCIA);
				String ticket = "Tarjeta rechazada  TAG: " + transaccion.getDestino()
						+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
				transaccion.setBitTicket(ticket);
				transaccion.setBitStatus(2);
				transaccion.setBitNoAutorizacion(REFERENCIA);
				transaccion.setBitCodigoError(-1);
				actualizaBitacora(transaccion, REFERENCIA, "-1", 0, response);
			}
		}catch(Exception e){
			mav = new ModelAndView("payworks/iaveError");
			mav.addObject("descError", 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
					+ "Por favor vuelva a intentarlo en unos minutos.");
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			datosResp = new ResponsePagoMovilesVO(100, 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
							+ "Por favor vuelva a intentarlo en unos minutos.");
		}finally{
			modelo.put("datosResp", datosResp);
			mav.addObject("datosResp", datosResp);
		}
		return mav;	
	}
	
	
	
	public ModelAndView procesaRespuestaProsa(String eM_Response,
			String eM_Total, String eM_OrderID, String eM_Merchant,
			String eM_Store, String eM_Term, String eM_RefNum, String eM_Auth,
			String eM_Digest) {
		ModelAndView mav = null;
		RespuestaServicioVO respuesta = null;
		TBitacoraVO transaccion = null;
		ReloadResponse response = null;
		try {
			LOGGER.debug("RESPUESTA 3DSECURE PARA LA TRANSACCION - " + eM_OrderID +" EM_Total: " + eM_Total 
					+" EM_Merchant: " + eM_Merchant +" EM_RefNum: " + eM_RefNum + " EM_Auth: " + eM_Auth
					+" eM_Response: "+eM_Response+" eM_Store: "+eM_Store+" eM_Term: "+eM_Term
					+" eM_Digest: "+eM_Digest);
			respuesta = new RespuestaServicioVO();
			transaccion = mapper.getDatosTransaccion(Long.valueOf(eM_OrderID));
			transaccion.setIdBitacora(Long.valueOf(eM_OrderID));
			if (!eM_Auth.equals("000000") || eM_Auth.equals("A")) {
				LOGGER.debug("TRANSACCION - " + eM_OrderID+" Aprobada - AUTORIZACION - "+eM_Auth);
				double total = 0;
				String decimales = eM_Total.substring(eM_Total.length() -2, eM_Total.length());
				String enteros = eM_Total.substring(0, eM_Total.length() -2);
				enteros = enteros+"."+decimales;
				total = Double.valueOf(enteros);
				total = transaccion.getCargo() + transaccion.getComision();
				response = aprovisionaIAve(transaccion);
				if("00".equals(response.getRESPONSECODE())){
					String saldoActual = getCheckBalanceTag(transaccion.getDestino(), transaccion.getIdUsuario());
					mav = new ModelAndView("3ds/pago");
					mav.addObject("respuesta", "Aprobado");
					mav.addObject("idTransaccion", eM_OrderID);	
					mav.addObject("autorizacion", eM_Auth);		
					mav.addObject("importe", transaccion.getCargo());
					mav.addObject("comision", transaccion.getComision());
					mav.addObject("saldoActual", saldoActual);
					mav.addObject("total", total);
					mav.addObject("descripcionCode", response.getDESCRIPTIONCODE());
					mav.addObject("tag", response.getCARD_NUMBER());
					mav.addObject("fecha", UtilsService.getFechaActual());
					String ticket = "COMPRA TAG I+D AUTORIZACION: " + response.getAUTONO() + " TAG: " + transaccion.getDestino()
							+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
					transaccion.setBitTicket(ticket);
					transaccion.setBitStatus(1);
					transaccion.setBitNoAutorizacion(eM_Auth);
					
					actualizaBitacora(transaccion, eM_Auth, "0", 1, response);
					enviaCorreo(transaccion, response, total, eM_Auth, eM_OrderID, eM_Merchant);
				} else {
					mav = new ModelAndView("3ds/error");
					respuesta.setIdError(-9);
					respuesta.setMsgError("Rechazo IAVE - ERROR: "+response.getRESPONSECODE()+ " TAG: " + transaccion.getDestino());
					String ticket = "COMPRA TAG I+D ERROR: " + response.getRESPONSECODE() + " TAG: " + transaccion.getDestino()
							+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
					transaccion.setBitTicket(ticket);
					transaccion.setBitStatus(3);
					transaccion.setBitNoAutorizacion(eM_Auth);
					transaccion.setBitCodigoError(-1);
					actualizaBitacora(transaccion, eM_Auth, "-1", 0, response);
				}
			} else {
				respuesta.setIdError(-9);
				respuesta.setMsgError("Tarjeta rechazada, " + eM_Response + " - " + eM_RefNum);
				LOGGER.error("Error Pago 3D Secure: " + eM_Response + " - " + eM_RefNum);
				String ticket = "Tarjeta rechazada  TAG: " + transaccion.getDestino()
						+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
				transaccion.setBitTicket(ticket);
				transaccion.setBitStatus(2);
				transaccion.setBitNoAutorizacion(eM_Auth);
				transaccion.setBitCodigoError(-1);
				actualizaBitacora(transaccion, eM_Auth, "-1", 0, response);
			}
		} catch (Exception e) {
			respuesta.setIdError(-100);
			respuesta.setMsgError("Ocurrio un error, problemas internos -3");
			LOGGER.error("Error al realizar la recarga", e);
			actualizaBitacora(transaccion, eM_Auth, "-1", 0, null);
		} finally {
			if (respuesta.getIdError() != 0) {
				mav = new ModelAndView("3ds/error");
				mav.addObject("descError", respuesta.getMsgError());
			}
		}
		return mav;
	}
	
	public ModelAndView procesaCobroDeudor(String eM_Response, String eM_Total, String eM_OrderID, String eM_Merchant,
			String eM_Store, String eM_Term, String eM_RefNum, String eM_Auth, String eM_Digest) {
		ModelAndView mav = null;
		RespuestaServicioVO respuesta = null;
		TBitacoraVO transaccion = null;
		ReloadResponse response = null;
		try {
			LOGGER.debug("COBRO DEUDORES: RESPUESTA 3DSECURE PARA LA TRANSACCION - " + eM_OrderID +" EM_Total: " + eM_Total 
					+" EM_Merchant: " + eM_Merchant +" EM_RefNum: " + eM_RefNum + " EM_Auth: " + eM_Auth
					+" eM_Response: "+eM_Response+" eM_Store: "+eM_Store+" eM_Term: "+eM_Term
					+" eM_Digest: "+eM_Digest);
			respuesta = new RespuestaServicioVO();
			transaccion = mapper.getDatosTransaccion(Long.valueOf(eM_OrderID));
			transaccion.setIdBitacora(Long.valueOf(eM_OrderID));
			if (!eM_Auth.equals("000000") || eM_Auth.equals("A")) {
				mav = new ModelAndView("3ds/pago");
				LOGGER.debug("TRANSACCION - " + eM_OrderID+" Aprobada - AUTORIZACION - "+eM_Auth);
				double total = 0;
				String decimales = eM_Total.substring(eM_Total.length() -2, eM_Total.length());
				String enteros = eM_Total.substring(0, eM_Total.length() -2);
				enteros = enteros+"."+decimales;
				total = Double.valueOf(enteros);
				total = transaccion.getCargo() + transaccion.getComision();
				response = new ReloadResponse();
				response.setAMOUNT(String.valueOf(total));
				response.setAUTONO(eM_Auth);
				response.setCARD_NUMBER(transaccion.getDestino());
				response.setCHECK_DIGIT("0");
				response.setDESCRIPTIONCODE("RECARGA VALIDA");
				response.setID_GRP("0000");
				response.setRESPONSECODE("00");
				response.setTRX_NO("1958581");
				Calendar mC = Calendar.getInstance();
				String wfecha = agregaCeros(new StringBuilder().append("").append(mC.get(5)).toString(), 2) + "/"
						+ agregaCeros(new StringBuilder().append("").append(mC.get(2) + 1).toString(), 2) + "/" + mC.get(1) + " "
						+ agregaCeros(new StringBuilder().append("").append(mC.get(11)).toString(), 2) + ":"
						+ agregaCeros(new StringBuilder().append("").append(mC.get(12)).toString(), 2) + ":"
						+ agregaCeros(new StringBuilder().append("").append(mC.get(13)).toString(), 2);
				response.setLOCAL_DATE(wfecha);
				transaccion.setBitTicket("AJUSTE SALDO IAVE TAG: "+transaccion.getDestino());
				transaccion.setBitStatus(1);
				transaccion.setBitNoAutorizacion(eM_Auth);
				actualizaBitacora(transaccion, eM_Auth, "0", 1, response);
				mapper.actualizaDeudor(eM_Auth, eM_OrderID);
				mav.addObject("respuesta", "Aprobado");
				mav.addObject("idTransaccion", eM_OrderID);	
				mav.addObject("autorizacion", eM_Auth);		
				mav.addObject("importe", transaccion.getCargo());
				mav.addObject("comision", transaccion.getComision());
				mav.addObject("saldoActual", total);
				mav.addObject("total", total);
				mav.addObject("descripcionCode", response.getDESCRIPTIONCODE());
				mav.addObject("tag", response.getCARD_NUMBER());
				mav.addObject("fecha", UtilsService.getFechaActual());
//				enviaCorreo(transaccion, response, total, eM_Auth, eM_OrderID, eM_Merchant);
			} else {
				respuesta.setIdError(-9);
				respuesta.setMsgError("Tarjeta rechazada, " + eM_Response + " - " + eM_RefNum);
				LOGGER.error("COBRO DEUDORES:  Error Pago 3D Secure: " + eM_Response + " - " + eM_RefNum);
				String ticket = "Tarjeta rechazada  TAG: " + transaccion.getDestino()
						+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
				transaccion.setBitTicket(ticket);
				transaccion.setBitStatus(2);
				transaccion.setBitNoAutorizacion(eM_Auth);
				transaccion.setBitCodigoError(-1);
				actualizaBitacora(transaccion, eM_Auth, "-1", 0, response);
			}
		} catch (Exception e) {
			respuesta.setIdError(-100);
			respuesta.setMsgError("Ocurrio un error, problemas internos -3");
			LOGGER.error(" COBRO DEUDORES: Error al realizar la recarga", e);
			actualizaBitacora(transaccion, eM_Auth, "-1", 0, null);
		} finally {
			if (respuesta.getIdError() != 0) {
				mav = new ModelAndView("3ds/error");
				mav.addObject("descError", respuesta.getMsgError());
			}
		}
		return mav;
	}

	private String getCheckBalanceTag(String tag, String idUsuario) {
		String saldo = null;
		BalanceResponse response = null;
		try {			
			RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
        		HttpEntity<String> request = new HttpEntity<String>("{\"tag\":\""+tag+"\"}", headers);
        		response = restTemplate.postForObject(URL_CHECK_BALANCE.replace("{idUsuario}", String.valueOf(idUsuario)), request, BalanceResponse.class);
			saldo = String.valueOf(response.getBalance());
			LOGGER.info("CHECK BALANCE  RESPONSE - {}", saldo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return saldo;
	}

	private void enviaCorreo(TBitacoraVO transaccion, ReloadResponse response, double total,
			String eM_Auth, String eM_OrderID, String eM_Merchant) {
		try {
			java.util.Date fecha = new java.util.Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"dd/MM/yyyy HH:mm:ss");
			DatosCorreoVO datosCorreoVO = new DatosCorreoVO();
			
			String emailBody = mapper.getParametro("@MENSAJE_COMPRAIAVE");
			String eMailSubject = mapper.getParametro("@ASUNTO_COMPRAIAVE").toString();
			datosCorreoVO.setNombre(transaccion.getNombre());
			datosCorreoVO.setUsuario(transaccion.getLogin());
			datosCorreoVO.setMonto(transaccion.getCargo());
			datosCorreoVO.setAutorizacionIave(response.getAUTONO());
			datosCorreoVO.setAutorizacion(eM_Auth);
			datosCorreoVO.setComision(transaccion.getComision());
			datosCorreoVO.setFecha(dateFormat.format(fecha));
			datosCorreoVO.setTagIave(transaccion.getDestino());
			datosCorreoVO.setEmail(transaccion.getMail());
			datosCorreoVO.setIdBitacora(transaccion.getIdBitacora());	
			AddCelGenericMail.generatedMailIave(datosCorreoVO, emailBody, eMailSubject);
			
		} catch (Exception e) {
			LOGGER.error("EnviaMail: " + e);
		}

	}

	public ReloadResponse aprovisionaIAve(TBitacoraVO transaccion){
		ReloadResponse response = new ReloadResponse();
		String idcom, idgrp, res = null;
		try {
			Calendar mC = Calendar.getInstance();
			String wfecha = agregaCeros(new StringBuilder().append("").append(mC.get(5)).toString(), 2) + "/"
					+ agregaCeros(new StringBuilder().append("").append(mC.get(2) + 1).toString(), 2) + "/" + mC.get(1) + " "
					+ agregaCeros(new StringBuilder().append("").append(mC.get(11)).toString(), 2) + ":"
					+ agregaCeros(new StringBuilder().append("").append(mC.get(12)).toString(), 2) + ":"
					+ agregaCeros(new StringBuilder().append("").append(mC.get(13)).toString(), 2);
			String prefijo = "";
			String puntos = "";
			if (transaccion.getDestino().length() != 11) {
				while (transaccion.getDestino().length() < 8) {
					transaccion.setDestino("0" + transaccion.getDestino());
				}
				if ((transaccion.getDestino().length() == 8) && (Long.parseLong(transaccion.getDestino()) >= 20000000L)) {
					prefijo = "IMDM";
				}
				if ((transaccion.getDestino().length() == 8) && (Long.parseLong(transaccion.getDestino()) < 20000000L)) {
					prefijo = "CPFI";
				}
				if (transaccion.getDestino().length() == 8) {
					puntos = "..";
				}
			}
			if (!esTuTag(String.valueOf(transaccion.getIdProducto()))) {
				idcom = mapper.getParametro("@ID_COM_IAVE");
				idgrp = mapper.getParametro("@ID_GRP_IAVE");
			} else {
				idcom = mapper.getParametro("@ID_COM_IAVE_TUTAG");
				idgrp = mapper.getParametro("@ID_GRP_IAVE_TUTAG");
			}
			String tipoTarjeta = "";
			if("1".equals(transaccion.getIdTipoTarjeta())){
				tipoTarjeta = "04";
			} else {
				tipoTarjeta = "28";
			}
			StringBuffer pXML = new StringBuffer().append("<ReloadRequest>").append("  <ID_COM>").append(idcom)
					.append("</ID_COM>").append("  <ID_GRP>").append(idgrp).append("</ID_GRP>")
					.append("  <CARD_NUMBER>").append(prefijo).append(transaccion.getDestino()).append(puntos).append("</CARD_NUMBER>")
					.append(" <CHECK_DIGIT>").append(transaccion.getPin()).append("</CHECK_DIGIT>").append("  <LOCAL_DATE>")
					.append(wfecha).append("</LOCAL_DATE>").append("  <AMOUNT>").append(transaccion.getCargo()).append("</AMOUNT>")
					.append(" <TRX_NO>").append(transaccion.getFolio()).append("</TRX_NO>")
					.append(" <METPAG>").append(tipoTarjeta).append("</METPAG>").append("</ReloadRequest>");
			LOGGER.info("XML ENVIADO A IAVE: "+pXML.toString());
			ObjectFactory factory = new ObjectFactory();
			Reload reload = factory.createReload();
			reload.setSXML(pXML.toString());
//			iaveResponse = (ReloadResponse) webServiceTemplate.marshalSendAndReceive("http://200.57.40.42/wsPrepagoRecargas/Reload", reload);
//			webServiceTemplate.setDefaultUri("http://200.57.40.42/wsPrepagoRecargas/Reload");
//			iaveResponse = (ReloadResponse) webServiceTemplate.marshalSendAndReceive(reload);
//			iaveResponse = (ReloadResponse) webServiceTemplate.marshalSendAndReceive(reload, 
//					new SoapActionCallback("http://200.57.40.42/wsPrepagoRecargas/Reload"));
			WsPrepagoRecargasSoapProxy proxy = new WsPrepagoRecargasSoapProxy();
			res = proxy.reload(pXML.toString());
			ParseIaveResponse parseIave = new ParseIaveResponse();
			response = parseIave.parseResponseXStream(res);
			LOGGER.info("RESPUESTA DE IAVE: ID TRANSACCION: "+transaccion.getIdBitacora()
					+" IAVE: ID_GRP: "+response.getID_GRP()+" CARD NUMBER: "+response.getCARD_NUMBER()
					+" AMOUNT: "+response.getAMOUNT()+" TRX_NO: "+response.getTRX_NO()+" AUTONO: "+response.getAUTONO()
					+" RESPONSE CODE: "+response.getRESPONSECODE()+" DESCRIPCION CODE: "+response.getDESCRIPTIONCODE());
		} catch (Exception e) {
			LOGGER.info("ERROR AL APROVISIONAR LA TRANSACCION IAVE: "+transaccion.getIdBitacora()+" CAUSA: "+e.getMessage());
			LOGGER.error("ERROR: "+e.getLocalizedMessage());
			response.setRESPONSECODE("-01");
			response.setDESCRIPTIONCODE("Error al comunicarse con IAVE");
		}
		return response;
	}
	
	public boolean esTuTag(String idProducto) {
		String productosTuTag = null;
		String[] productosTuTagArr = null;
		boolean resultado = false;
		try {
			productosTuTag = mapper.getParametro("@PRODUCTOS_TUTAG");
			if(productosTuTag != null){
				productosTuTagArr = productosTuTag.split("\\|");
				for (String s : productosTuTagArr) {
					if (s.equalsIgnoreCase(idProducto)) {
						resultado = true;
						break;
					}
				}	
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar es TU TAG: {}", e);
		}
		return resultado;
	}
	
	private void actualizaBitacora(TBitacoraVO bitacora , String eM_Auth, String string,
			int i, ReloadResponse response) {
		try {
			LOGGER.info("ACTUALIZANDO TRANSACCION - {}", bitacora.getIdBitacora());
			mapper.actualizaBitacora(bitacora);
			if(response != null){
				response.setIdBitacora(bitacora.getIdBitacora());
				mapper.actualizaBitacoraIAVE(response);
				bitacora.setTarjetaIave(response.getTRX_NO()+"-"+response.getAUTONO());
			}
			mapper.actualizaBitacoraProsa(bitacora);
		} catch (Exception e) {
			LOGGER.error("ERROR AL ACTUALIZAR LAS BITACORAS - CAUSA: "+e.getCause());
			e.printStackTrace();
		}
	}

	private String agregaCeros(String cad, int NumCeros) {
		String tmp = "00000000000000000000000000000";
		return (tmp + cad).substring((tmp + cad).length() - NumCeros, (tmp + cad).length());
	}
	
	private AbstractVO validaBloqueos(String tarjeta, String imei) {
		AbstractVO response = new AbstractVO();
		int bloqueoTag, bloqueoImei = 0;
		bloqueoTag = mapper.getInfoTag(tarjeta);
		bloqueoImei = mapper.getInfoImei(imei);
		if(bloqueoTag != 0){
			response.setIdError(-1);
			response.setMensajeError("El tag se encuentra bloqueado.");
		} else if(bloqueoImei != 0){
			response.setIdError(-1);
			response.setMensajeError("El imei se encuentra bloqueado.");
		}
		return response;
	}

	private AbstractVO validaBloqueos(String tarjeta) {
		AbstractVO response = new AbstractVO();
		int bloqueoTarjeta = 0;
		int bine = 0;
		bloqueoTarjeta = mapper.getBloqueoTarjeta(tarjeta);
		bine = mapper.getValidaBines(tarjeta);
		if(bloqueoTarjeta != 0){
			response.setIdError(-1);
			response.setMensajeError("Estimado usuario, hemos detectado actividad sospechosa por lo cual su tarjeta se encuentra bloqueada.");
		} 
		if(bine != 0){
			response.setIdError(-1);
			response.setMensajeError("Estimado usuario, hemos detectado actividad sospechosa por lo cual su tarjeta se encuentra bloqueada.");
		} 
		return response;
	}
	
	private void insertaBitacoras(Integer idApp, IaveVO compra, UsuarioVO usuario, Producto producto) {
		TBitacoraVO bitacora = new TBitacoraVO();
		bitacora.setTipo(compra.getTipo());
		bitacora.setSoftware(compra.getSoftware());
		bitacora.setModelo(compra.getModelo());
		bitacora.setWkey(compra.getKey());
		bitacora.setIdUsuario(String.valueOf(usuario.getIdUsuario()));
		bitacora.setIdProducto(Integer.valueOf(producto.getClave()));
		bitacora.setCar_id(Integer.parseInt(producto.getClaveWS()));
		bitacora.setCargo(Double.parseDouble(producto.getMonto()));
		bitacora.setIdProveedor(Integer.valueOf(producto.getProveedor()));
		bitacora.setConcepto("Compra I + D -  TAG: " + compra.getTarjeta() + " "
				+ producto.getMonto() + " Comision: " + compra.getComision());
		bitacora.setImei(compra.getImei());
		bitacora.setDestino(compra.getTarjeta());
		bitacora.setTarjeta_compra(usuario.getNumTarjeta());
		bitacora.setAfiliacion(Constantes.MERCHANT);
		bitacora.setComision(compra.getComision());
		bitacora.setCx(compra.getCx());
		bitacora.setCy(compra.getCy());
		bitacora.setPin(compra.getPin());
		bitacora.setIdApp(idApp);
		mapper.addBitacora(bitacora);
		mapper.addBitacoraIAVE(bitacora);
		mapper.addBitacoraProsa(bitacora);
		compra.setIdBitacora(bitacora.getIdBitacora());
	}
	
	private ReglasResponse validaReglas(long idUsuario, String tag, int idProducto, String card, String imei, int idTarjeta, int amex) {
		ReglasResponse response = null;
		ValidacionVO validacion = null;
		String val = null;
		try {			
			val = mapper.validaIAVE(idUsuario, "8", tag, imei, idTarjeta, AddcelCrypto.decryptTarjeta(card).substring(0, 6), amex);
			LOGGER.info("IAVE - VALIDACION REGLAS - {}", val);
			validacion = (ValidacionVO) jsonUtils.jsonToObject(val, ValidacionVO.class);
			if(validacion.getIdError() != 0){
				response = new ReglasResponse(0, "920",validacion.getMensajeError());
			}
			LOGGER.info("VALIDANDO TARJETA NO PERMITIDA...");
			if(AddcelCrypto.decryptTarjeta(card).startsWith("42680702")) {
				LOGGER.info("TARJETA NO PERMITIDA...");
				response = new ReglasResponse(0, "930", "Tarjeta no permitida. Por su seguridad se esta validando su tarjeta con el Banco. ");
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar Reglas de Negocio.", e);
			response = new ReglasResponse(0, "920", "Ocurrio un error al validar Reglas de Negocio.");
		}
		LOGGER.info("RESPUESTA DE VALIDACION - {}", jsonUtils.objectToJson(response));
		return response;
	}

    public static String setSMS(String Telefono){
        return Crypto.aesEncrypt(parsePass(TELEFONO_SERVICIO),  Telefono);    
    }
	
    public static String parsePass(String pass){
        int len = pass.length();
        String key = "";

        for (int i =0; i < 32 /len; i++){
              key += pass;
        }

        int carry = 0;
        while (key.length() < 32){
              key += pass.charAt(carry);
              carry++;
        }
        return key;
    }
    
    public static String formatoImporte(double numDouble){
        String total = null;
        try{
            total = DF.format(numDouble);
        }catch(Exception e){
            LOGGER.error("Error en formatoImporte: "+ e.getMessage());
        }
        return total;
    }

}